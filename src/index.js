const { GraphQLServer } = require('graphql-yoga')
const { prisma } = require('./generated/prisma-client')

// Todo: Refactor importing after typescript integration
const Query = require('./graphql/resolvers/Query')
const Mutation = require('./graphql/resolvers/Mutation')
const Subscription = require('./graphql/resolvers/Subscription')
const User = require('./graphql/resolvers/User')
const Link = require('./graphql/resolvers/Link')
const Vote = require('./graphql/resolvers/Vote')

const resolvers = {
    Query,
    Mutation,
    Subscription,
    User,
    Link,
    Vote,
}

const server = new GraphQLServer({
    typeDefs: './src/graphql/schema.graphql',
    resolvers,
    context: request => ({
        ...request,
        prisma,
    }),
})

server.start(() => console.log(`Server is running on http://localhost:4000`))
