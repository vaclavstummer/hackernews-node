const links = (root, args, context) =>
    context.prisma.user({ id: root.id }).links()

module.exports = {
    links,
}
